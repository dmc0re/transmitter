use std::fmt;

pub struct Angle {
  decimal_value: f64,
}

impl Angle {
  pub fn new(decimal_value: f64) -> Self {
    Self { decimal_value }
  }

  pub fn degrees(&self) -> i32 {
    self.decimal_value as i32
  }

  pub fn minutes(&self) -> f64 {
    (self.decimal_value - self.degrees() as f64).abs() * 0.6 * 100.
  }

  pub fn get_decimal_value(degree: i32, mins: f64) -> f64 {
    (degree as f64) + mins / 0.6 / 100.
  }
}

impl fmt::Display for Angle {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let first_path = format!("{:03}", self.degrees());

    let x: &[_] = &['0', '-'];
    write!(f, "{}{:05.2}", first_path.trim_start_matches(x), self.minutes())
  }
}
