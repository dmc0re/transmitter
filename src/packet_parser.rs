use super::log_record::LogRecord;

use crc_any::CRC;

pub struct PacketParser {
  crc8: CRC,
}

impl PacketParser {
  pub fn new() -> PacketParser {
    Self {
      crc8: CRC::create_crc(0x07, 8, 0, 0, false),
    }
  }

  pub fn try_to_get_from_data(&mut self, data: &[u8]) -> Option<LogRecord> {
    for &packet_size in [23, 27].iter() {
      if data.len() >= packet_size {
        let packet = &data[(data.len() - packet_size)..];

        self.crc8.reset();
        self.crc8.digest(&packet[..(packet.len() - 1)]);
        if (packet[packet.len() - 1] as u64) == self.crc8.get_crc() {
          return LogRecord::from_data(packet).ok();
        }
      }
    }

    None
  }
}

#[test]
fn it_gets_record_from_data_1() {
  let packet = [
    0xf3, 0x41, 0xaf, 0x45, 0x1b, 0x86, 0x7b, 0x45, 0x36, 0x90, 0x88, 0x5d, 0x9a, 0x19, 0x0d, 0x43, 0x12, 0xa5, 0xbd,
    0x3e, 0x31, 0x01, 0x28,
  ];

  let mut some_data = vec![0x00, 0x01, 0x02, 0x03];
  some_data.extend_from_slice(&packet);

  assert_eq!(some_data.len(), 27);

  let mut parser = PacketParser::new();
  assert!(parser.try_to_get_from_data(&some_data).is_some());
}

#[test]
fn it_gets_record_from_data_2() {
  let packet = [
    237u8, 65u8, 175u8, 69u8, 71u8, 134u8, 123u8, 69u8, 32u8, 59u8, 160u8, 93u8, 154u8, 25u8, 27u8, 67u8, 18u8, 165u8,
    61u8, 63u8, 49u8, 1u8, 211u8,
  ];

  let mut some_data = vec![0x00, 0x01, 0x02, 0x03];
  some_data.extend_from_slice(&packet);

  assert_eq!(some_data.len(), 27);

  let mut parser = PacketParser::new();
  assert!(parser.try_to_get_from_data(&some_data).is_some());
}
