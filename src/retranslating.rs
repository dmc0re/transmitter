use super::packet_parser::PacketParser;

use super::program::serial_port_settings;

use std::error::Error;
use std::io::prelude::*;
use std::io::{Error as IoError, ErrorKind, Read};
use std::net::TcpStream;
use std::thread;
use std::thread::JoinHandle;

pub const RETRANSLATING_MODE: &str = "retranslating";

pub fn run_retranslating_proc(
  serial_port_names: Vec<String>,
  ip_addr_and_port: Option<String>,
) -> Result<(), Box<dyn Error>> {
  println!("Starting program in \"{}\" mode...", RETRANSLATING_MODE);
  let mut threads = Vec::new();
  for serial_port_name in serial_port_names.into_iter() {
    let handler = start_serial_port_thread(serial_port_name, &ip_addr_and_port)?;
    threads.push(handler);
  }

  for thread in threads {
    thread.join().ok();
  }

  Ok(())
}

fn start_serial_port_thread(
  serial_port_name: String,
  ip_addr_and_port: &Option<String>,
) -> Result<JoinHandle<()>, Box<dyn Error>> {
  let ip_addr_and_port_copy = ip_addr_and_port.clone();

  match serialport::open_with_settings(&serial_port_name, &serial_port_settings()) {
    Ok(mut port) => Ok(thread::spawn(move || {
      println!("[{}] thread is starting...", serial_port_name);

      let mut received_data = Vec::new();
      let mut packet_parser = PacketParser::new();
      let mut receive_buffer: Vec<u8> = vec![0; 64];

      let thread_error = loop {
        match port.read(&mut receive_buffer) {
          Ok(received_data_size) => {
            received_data.extend_from_slice(&receive_buffer[..received_data_size]);

            if let Some(log) = packet_parser.try_to_get_from_data(received_data.as_slice()) {
              println!(
                "[{}] has received a packet: cid: {}, ts: {}",
                serial_port_name,
                log.controller_id(),
                log.date_time()
              );

              if let Some(ip_addr_and_port_string) = ip_addr_and_port_copy.clone() {
                match TcpStream::connect(ip_addr_and_port_string).as_mut() {
                  Ok(stream) => {
                    if let Err(e) = stream.write(&received_data[(received_data.len() - 23)..]) {
                      println!("Failed to wtire to tcp stream. Error: {}", e);
                    }
                  }
                  Err(e) => println!("Failed to open tcp stream. Error: {}", e),
                }
              }

              received_data.clear();
            }
          }
          Err(ref e) if e.kind() == ErrorKind::TimedOut => (),
          Err(e) => break e,
        }
      };
      println!(
        "Error! Thread {} was unexpected finished with error: {}",
        serial_port_name, thread_error
      );
    })),
    Err(e) => {
      return Err(Box::new(IoError::new(
        ErrorKind::Other,
        format!("Failed to open \"{}\" port. Error: {}", serial_port_name, e),
      )));
    }
  }
}
