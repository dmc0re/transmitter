use super::log_record::LogRecord;
use super::packet_parser::PacketParser;

use super::program::serial_port_settings;

use std::error::Error;
use std::io::{Error as IoError, ErrorKind, Read};
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::JoinHandle;

use std::net::{TcpListener, TcpStream};

use mongodb::coll::options::IndexOptions;
use mongodb::coll::Collection;
use mongodb::{bson, doc};

pub const COLLECTING_MODE: &str = "collecting";

type SafeDbCollection = Arc<Mutex<Collection>>;

pub fn run_collecting_proc(
  db_collection: Collection,
  serial_port_names: Vec<String>,
  ip_addr_and_port: Option<String>,
) -> Result<(), Box<dyn Error>> {
  println!("Starting program in \"{}\" mode...", COLLECTING_MODE);

  let safe_db_collection = prepare_db_collection(db_collection)?;
  let mut threads = Vec::new();
  for serial_port_name in serial_port_names.into_iter() {
    let handler = start_serial_port_thread(serial_port_name, &safe_db_collection)?;
    threads.push(handler);
  }

  if let Some(tcp_params) = ip_addr_and_port {
    threads.push(start_tcp_thread(tcp_params, &safe_db_collection)?);
  }

  for thread in threads {
    thread.join().ok();
  }

  Ok(())
}

fn prepare_db_collection(db_collection: Collection) -> Result<SafeDbCollection, Box<dyn Error>> {
  let mut index_options: IndexOptions = Default::default();
  index_options.unique = Some(true);

  db_collection.create_index(doc! { "cid": 1, "ts": -1}, Some(index_options))?;

  Ok(Arc::new(Mutex::new(db_collection)))
}

fn start_serial_port_thread(
  serial_port_name: String,
  safe_db_collection: &SafeDbCollection,
) -> Result<JoinHandle<()>, Box<dyn Error>> {
  let collection_handler = safe_db_collection.clone();

  match serialport::open_with_settings(&serial_port_name, &serial_port_settings()) {
    Ok(mut port) => Ok(thread::spawn(move || {
      println!("[{}] thread is starting...", serial_port_name);

      let mut received_data = Vec::new();
      let mut packet_parser = PacketParser::new();
      let mut receive_buffer: Vec<u8> = vec![0; 64];

      let thread_error = loop {
        match port.read(&mut receive_buffer) {
          Ok(received_data_size) => {
            received_data.extend_from_slice(&receive_buffer[..received_data_size]);

            if let Some(log) = packet_parser.try_to_get_from_data(received_data.as_slice()) {
              store_received_log(log, &collection_handler, &serial_port_name);

              received_data.clear();
            }
          }
          Err(ref e) if e.kind() == ErrorKind::TimedOut => (),
          Err(e) => break e,
        }
      };
      println!(
        "Error! Thread {} was unexpected finished with error: {}",
        serial_port_name, thread_error
      );
    })),
    Err(e) => {
      return Err(Box::new(IoError::new(
        ErrorKind::Other,
        format!("Failed to open \"{}\" port. Error: {}", serial_port_name, e),
      )));
    }
  }
}

fn start_tcp_thread(
  ip_addr_and_port: String,
  safe_db_collection: &SafeDbCollection,
) -> Result<JoinHandle<()>, Box<dyn Error>> {
  println!("[TCP] thread is starting...");

  let collection_handler = safe_db_collection.clone();
  let listener = TcpListener::bind(&ip_addr_and_port)?;

  Ok(thread::spawn(move || {
    for stream in listener.incoming() {
      if let Ok(s) = stream {
        handle_tcp_client(s, &collection_handler)
      }
    }
  }))
}

fn handle_tcp_client(stream: TcpStream, safe_db_collection: &SafeDbCollection) {
  let mut s = stream;
  let mut receive_buffer: Vec<u8> = vec![0; 23];
  let mut packet_parser = PacketParser::new();

  match s.read_to_end(&mut receive_buffer) {
    Ok(23) => {
      if let Some(log) = packet_parser.try_to_get_from_data(&receive_buffer) {
        store_received_log(log, &safe_db_collection, &String::from("TCP"));
      }
    }
    Ok(_) => (),
    Err(e) => {
      println!("Failed to read from TCP stream. Error: {}", e);
    }
  }
}

fn store_received_log(log: LogRecord, safe_db_collection: &SafeDbCollection, channel_name: &String) {
  let collection = safe_db_collection.lock().unwrap();
  println!(
    "[{}] has received a packet: cid: {}, ts: {}",
    channel_name,
    log.controller_id(),
    log.date_time()
  );

  if let Err(e) = collection.insert_one(log.to_bson_doc(), None) {
    println!("Failed to insert a log. Error: {}", e)
  }
}
