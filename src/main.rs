mod angle;
mod collecting;
mod log_record;
mod packet_parser;
mod program;
mod resuming;
mod retranslating;

fn main() {
  match program::run() {
    Err(err) => {
      eprintln!("ERROR: {}", err);
    }
    _ => {}
  }
}
