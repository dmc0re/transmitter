use super::angle::Angle;

use byteorder::{LittleEndian, ReadBytesExt};
use std::collections::HashMap;
use std::io::prelude::*;
use std::io::{Cursor, Result, SeekFrom};

use bson::ordered::ValueAccessResult;
use mongodb::{bson, Bson};

use chrono::NaiveDateTime;

pub struct LogRecord {
  lat: f64,
  lon: f64,
  ts: i64,
  alt: f64,
  sp: f64,
  fx: i32,
  cid: i32,
}

impl LogRecord {
  pub fn controller_id(&self) -> i32 {
    self.cid
  }

  pub fn date_time(&self) -> NaiveDateTime {
    NaiveDateTime::from_timestamp(self.ts as i64, 0)
  }

  pub fn to_bson_doc(&self) -> bson::Document {
    let mut doc = bson::Document::new();

    doc.insert("lat", Bson::FloatingPoint(self.lat));
    doc.insert("lon", Bson::FloatingPoint(self.lon));
    doc.insert("ts", Bson::I64(self.ts));
    doc.insert("alt", Bson::FloatingPoint(self.alt));
    doc.insert("sp", Bson::FloatingPoint(self.sp));
    doc.insert("fx", Bson::I32(self.fx));
    doc.insert("cid", Bson::I32(self.cid));

    doc
  }

  fn to_gps_string(&self) -> String {
    let date_time = NaiveDateTime::from_timestamp(self.ts, 0);
    let gps_string = format!(
      "GPS,{},{},{},{},{},0,0,0,{},M,0,M,{},0.00,0,E,0,0,0,0,",
      date_time.format("%H%M%S"),
      date_time.format("%d%m%y"),
      if self.fx & 0x80 == 0 && self.fx & 0x7F != 48 {
        'A'
      } else {
        'V'
      },
      Self::latitude_to_string(self.lat),
      Self::longitude_to_string(self.lon),
      self.alt as i32,
      format!("{:.15}", self.sp / 1.852)
    );

    format!(
      "${}*{:x}",
      gps_string,
      gps_string.as_bytes().iter().fold(0, |c, x| c ^ *x)
    )
  }

  pub fn to_fm_log(&self, mapping: &HashMap<i32, i32>) -> String {
    let cid = mapping.get(&self.cid).unwrap_or(&self.cid);
    format!("Aircraft # {}\r\n{}\r\n$END\r\n", cid, self.to_gps_string())
  }

  pub fn from_data(data: &[u8]) -> Result<Self> {
    let mut reader = Cursor::new(data);
    Ok(Self {
      lat: reader.read_f32::<LittleEndian>()? as f64,
      lon: reader.read_f32::<LittleEndian>()? as f64,
      ts: reader.read_i32::<LittleEndian>()? as i64,
      alt: reader.read_f32::<LittleEndian>()? as f64,
      sp: if data.len() == 23 {
        reader.read_f32::<LittleEndian>()? as f64
      } else {
        reader.seek(SeekFrom::Current(8))?;
        Default::default()
      },
      fx: reader.read_u8()? as i32,
      cid: reader.read_u8()? as i32,
    })
  }

  pub fn from_bson_doc(doc: &bson::Document) -> ValueAccessResult<Self> {
    Ok(Self {
      lat: doc.get_f64("lat")?,
      lon: doc.get_f64("lon")?,
      ts: doc.get_i64("ts")?,
      alt: doc.get_f64("alt")?,
      sp: doc.get_f64("sp")?,
      fx: doc.get_i32("fx")?,
      cid: doc.get_i32("cid")?,
    })
  }

  fn latitude_to_string(lat: f64) -> String {
    let decimal_value = Angle::get_decimal_value((lat / 100.) as i32, lat % 100.);
    let angle = Angle::new(decimal_value);

    format!("{},{}", angle, if decimal_value >= 0. { "N" } else { "S" })
  }

  fn longitude_to_string(lat: f64) -> String {
    let decimal_value = Angle::get_decimal_value((lat / 100.) as i32, lat % 100.);
    let angle = Angle::new(decimal_value);

    format!("{},{}", angle, if decimal_value >= 0. { "E" } else { "W" })
  }
}

#[test]
fn it_properly_formats_fm_log() {
  let packet = vec![
    0xe4, 0x41, 0xaf, 0x45, 0x49, 0x86, 0x7b, 0x45, 0xb4, 0x4f, 0xa8, 0x5d, 0x66, 0x66, 0x0e, 0x43, 0x12, 0xa5, 0x3d,
    0x3e, 0x31, 0x01, 0xe5,
  ];

  let log = LogRecord::from_data(&packet).ok().unwrap();
  let mapping: HashMap<i32, i32> = [(1, 5)].iter().cloned().collect();

  assert_eq!(
    log.to_fm_log(&mapping),
    String::from(
      "Aircraft # 5\r\n\
       $GPS,112540,171019,A,5608.24,N,4024.39,E,0,0,0,142,M,0,M,0.100000003115405,0.00,0,E,0,0,0,0,*5f\r\n\
       $END\r\n"
    )
  );
}
