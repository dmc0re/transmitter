use super::log_record::LogRecord;

use std::error::Error;
use std::fs::OpenOptions;
use std::io::Write;
use std::{thread, time};

use std::collections::HashMap;

use bson::ordered::OrderedDocument;
use mongodb::coll::options::FindOptions;
use mongodb::coll::Collection;
use mongodb::{bson, doc, Bson};

pub const RESUMING_MODE: &str = "resuming";

pub fn run_resuming_proc(
  db_collection: Collection,
  output_file_path: String,
  mapping: HashMap<i32, i32>,
) -> Result<(), Box<dyn Error>> {
  println!("Starting program in \"{}\" mode...", RESUMING_MODE);

  let mut current_doc: Option<OrderedDocument> = None;
  loop {
    if let Some(last_doc) = current_doc.clone() {
      let mut opts = FindOptions::new();
      opts.sort = Some(doc! { "_id": 1 });

      match last_doc.get_object_id("_id") {
        Ok(last_doc_id) => {
          let mut filter_case = bson::Document::new();
          filter_case.insert("$gt", Bson::ObjectId(last_doc_id.clone()));
          let mut filter = bson::Document::new();
          filter.insert("_id", Bson::Document(filter_case));

          match db_collection.find(Some(filter), Some(opts)) {
            Ok(mut cursor) => {
              while let Some(record) = cursor.next() {
                match record {
                  Ok(doc) => {
                    if let Ok(log) = LogRecord::from_bson_doc(&doc) {
                      if write_to_file(&log, &output_file_path, &mapping).is_ok() {
                        print_dot();
                      }
                    }
                    current_doc = Some(doc);
                  }
                  Err(e) => ln_and_println(format!("Failed to get a doc from cursor, error: {}", e)),
                }
              }
            }
            Err(e) => ln_and_println(format!("Failed to get a cursor, error: {}", e)),
          }
        }
        Err(e) => ln_and_println(format!("Failed to get ObjectID for last doc, error: {}", e)),
      }
    } else {
      let mut opts = FindOptions::new();
      opts.sort = Some(doc! { "_id": -1 });
      match db_collection.find_one(None, Some(opts)) {
        Ok(record) => current_doc = record,
        Err(e) => ln_and_println(format!("Failed to find a document, error: {}", e)),
      }
    }

    thread::sleep(time::Duration::from_secs(1));
  }
}

fn ln_and_println(text: String) {
  println!("");
  print!("{}", text);
}

fn print_dot() {
  print!(".");
  std::io::stdout().flush().unwrap();
}

fn write_to_file(log: &LogRecord, path: &String, mapping: &HashMap<i32, i32>) -> Result<(), ()> {
  match OpenOptions::new().append(true).create(true).open(&path) {
    Ok(mut file) => match file.write_all(log.to_fm_log(&mapping).as_bytes()) {
      Err(e) => println!("Failed to write to file. Error: {}", e),
      _ => (),
    },
    Err(e) => println!("Failed to open a file for writing. Error: {}", e),
  }

  Ok(())
}
