use super::collecting::*;
use super::resuming::*;
use super::retranslating::*;

use std::collections::HashMap;
use std::error::Error;
use std::io::{Error as IoError, ErrorKind};
use std::time::Duration;

use mongodb::coll::Collection;
use mongodb::db::ThreadedDatabase;
use mongodb::{Client, ThreadedClient};

use config::{Config, File};

use serialport::SerialPortSettings;

pub fn run() -> Result<(), Box<dyn Error>> {
  let settings = read_settings()?;
  let (mode, db_collection) = common_settings(&settings)?;

  match mode.as_str() {
    RESUMING_MODE => run_resuming_proc(db_collection, output_file_path(&settings)?, mapping(&settings)?)?,
    COLLECTING_MODE => run_collecting_proc(db_collection, serial_port_names(&settings)?, tcp_params(&settings)?)?,
    RETRANSLATING_MODE => run_retranslating_proc(serial_port_names(&settings)?, tcp_params(&settings)?)?,
    _ => {
      return Err(Box::new(IoError::new(
        ErrorKind::InvalidData,
        format!(
          "Unknown mode {}. Available modes: \"{}\", \"{}\", \"{}\".",
          mode, RESUMING_MODE, COLLECTING_MODE, RETRANSLATING_MODE
        ),
      )))
    }
  };

  Ok(())
}

pub fn serial_port_settings() -> SerialPortSettings {
  let mut settings: SerialPortSettings = Default::default();
  settings.timeout = Duration::from_millis(10);
  settings.baud_rate = 9600;

  settings
}

fn read_settings() -> Result<Config, Box<dyn Error>> {
  let mut settings = Config::default();
  settings.merge(File::with_name("config.toml"))?;

  Ok(settings)
}

fn db_collection(settings: &config::Config) -> Result<Collection, Box<dyn Error>> {
  let host = settings.get_str("mongodb.host")?;
  let port = settings.get_int("mongodb.port")?;
  let db_name = settings.get_str("mongodb.db_name")?;
  let collection_name = settings.get_str("mongodb.collection")?;

  let mongo_client = Client::connect(&host, port as u16)?;
  let collection = mongo_client.db(&db_name).collection(&collection_name);

  Ok(collection)
}

fn common_settings(settings: &Config) -> Result<(String, Collection), Box<dyn Error>> {
  Ok((settings.get_str("mode")?, db_collection(&settings)?))
}

fn serial_port_names(settings: &Config) -> Result<Vec<String>, Box<dyn Error>> {
  Ok(
    settings
      .get_array(&format!("{}.serial_ports", COLLECTING_MODE))?
      .into_iter()
      .filter_map(|v| v.into_str().ok())
      .collect(),
  )
}

fn output_file_path(settings: &Config) -> Result<String, Box<dyn Error>> {
  Ok(settings.get_str(&format!("{}.output_file_path", RESUMING_MODE))?)
}

fn tcp_params(settings: &Config) -> Result<Option<String>, Box<dyn Error>> {
  if settings.get_bool(&format!("{}.use_tcp_thread", COLLECTING_MODE))? {
    let addr = settings.get_str(&format!("{}.ip_addr", COLLECTING_MODE))?;
    let port = settings.get_int(&format!("{}.port", COLLECTING_MODE))?;

    Ok(Some(format!("{}:{}", addr, port)))
  } else {
    Ok(None)
  }
}

fn mapping(settings: &Config) -> Result<HashMap<i32, i32>, Box<dyn Error>> {
  Ok(
    settings
      .get_array(&format!("{}.mapping", RESUMING_MODE))?
      .into_iter()
      .fold(HashMap::new(), |mut result, map| {
        if let Ok(cid_aid_array) = map.into_array() {
          let cid_aid: Vec<i64> = cid_aid_array.into_iter().filter_map(|v| v.into_int().ok()).collect();
          if cid_aid.len() == 2 {
            result.insert(cid_aid[0] as i32, cid_aid[1] as i32);
          }
        }
        result
      }),
  )
}
